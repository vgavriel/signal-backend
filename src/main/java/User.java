package main.java;

public class User {
	
	private Sender sender;
	private long phoneNum;
	
	public User(long phoneNum){
		this.phoneNum = phoneNum;
		this.sender = new Sender(phoneNum);
	}

}
