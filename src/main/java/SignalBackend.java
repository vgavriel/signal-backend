package main.java;

import java.util.HashMap;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;

public class SignalBackend {
	
	private HashMap<Long, User> users;
	
	private SignalBackend(){
		users = new HashMap<>();
		
	}
	
	public static void main(String[] args){
		
		SignalBackend signal = new SignalBackend();
		
		signal.populateUsers();
		
		ContactSearch contSearch = new ContactSearch(signal.users);
		
		contSearch.contactSearch();
		
		
	}
	
	private void populateUsers(){
		String url = "https://signal-app.firebaseio.com/users/";
		Firebase dataRef = new Firebase(url);

		dataRef.addChildEventListener(new ChildEventListener() {
			
			@Override
			public void onChildChanged(DataSnapshot snapshot, String previousChildName) {
			}

			@Override
			public void onCancelled() {
				System.err.println("Listener was cancelled");
			}

			@Override
			public void onChildAdded(DataSnapshot snapshot, String arg1) {
				try{
					long userTel = Long.parseLong(snapshot.getName());				
					users.put(userTel, (new User(userTel)));
				} catch (NumberFormatException e){
					System.err.println("Username Not A Number: " + snapshot.getName());
				}
			}

			@Override
			public void onChildMoved(DataSnapshot arg0, String arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onChildRemoved(DataSnapshot arg0) {
				// TODO Auto-generated method stub
				
			}
		});

	
	}

}
