package main.java;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.GenericTypeIndicator;

public class Sender {
	
	private long _phoneNumber;
	
	public Sender(long number){
		_phoneNumber = number;
		String fireBaseURL = "https://signal-app.firebaseio.com/users/"+_phoneNumber+"/sent";
		final Firebase ref = new Firebase(fireBaseURL);
		ref.addChildEventListener(new ChildEventListener(){
			
			
			@Override
			public void onCancelled(){
				
			}

			@Override
			public void onChildAdded(DataSnapshot snapshot, String arg1) {
				
				final String messageKey = snapshot.getName();
				GenericTypeIndicator<Map<String, Object>> t = new GenericTypeIndicator<Map<String, Object>>(){};
				final Map<String,Object> message = snapshot.getValue(t);
				
				String findFollowersURL = "https://signal-app.firebaseio.com/users/"+_phoneNumber;
				Firebase followers = new Firebase(findFollowersURL);
				
				
				followers.addChildEventListener(new ChildEventListener(){

					@Override
					public void onCancelled() {						
					}

					@Override
					public void onChildAdded(DataSnapshot snapshot, String arg1) {
						if(snapshot.getName().equals("followers")){
							//System.out.println("Test: "+snapshot.getValue());
							String str = (String) snapshot.getValue();
							List<String> followersList = Arrays.asList(str.split(","));
							Firebase users = new Firebase("https://signal-app.firebaseio.com/users");
							for(String user:followersList){
								users.child(user).child("inbox").child(messageKey).setValue(message);
							}
							
						}
					
						
					}

					@Override
					public void onChildChanged(DataSnapshot arg0, String arg1) {
						
					}

					@Override
					public void onChildMoved(DataSnapshot arg0, String arg1) {
						
						
					}

					@Override
					public void onChildRemoved(DataSnapshot arg0) {
						
						
					}
					
				});
				ref.child(messageKey).removeValue();
				
			}

			@Override
			public void onChildChanged(DataSnapshot arg0, String arg1) {
				
				
			}

			@Override
			public void onChildMoved(DataSnapshot arg0, String arg1) {
				
				
			}

			@Override
			public void onChildRemoved(DataSnapshot arg0) {
				
				
			}
		});
	}	

}
